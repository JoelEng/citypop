import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useParams, useHistory } from 'react-router-dom'
import toast from 'react-hot-toast';
import React from 'react';

//This class is used for SearchCity and SearchCountry to accomplish DRY code.
const Search = () => {
    const [value, setValue] = React.useState('');
    const { type } = useParams();

    //This keeps value up-to-date with the value in the input field
    const onChange = event => setValue(event.target.value)

    const history = useHistory();
    const handleClick = () => {
        if (value === '') {
            toast.error('Please enter a ' + type);
        } else {
            history.push("/" + type + "result/" + value)
        }
    }

    const handleKeyPress = e => {
        if (e.code === 'Enter') {
            handleClick();
        }
    }
    
    return (
        <div className="element-col">
            <p>
                {/* this.props.children is passed from the subclasses. This is a string that will be either "city" or "country" */}
                SEARCH BY {type.toUpperCase()}
            </p>

            <input
                type="text"
                placeholder={"Enter a " + type}
                value={value}
                onChange={onChange}
                onKeyPress={handleKeyPress}
                autoFocus
            />

            {/* This links to the city result page */}
            <button className="search-button" type="submit" onClick={handleClick}>
                <FontAwesomeIcon icon={faSearch} />
            </button>
        </div>
    )
}

export default Search;