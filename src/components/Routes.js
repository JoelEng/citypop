import React from 'react';
import { Switch, Route } from 'react-router-dom';

import MainPage from './MainPage';
import CityResult from './CityResult';
import CountryResult from './CountryResult';
import Search from './Search';

const Routes = () => {
    return (
        //This routing table creates the foundation for link-handling on the website
        <Switch> 
            <Route exact path='/' component={MainPage}></Route>
            <Route exact path='/search:type' component={Search}></Route>
            <Route exact path='/countryresult/:query' component={CountryResult}></Route>
            <Route exact path='/cityresult/:query' component={CityResult}></Route>
        </Switch>
    );
}

export default Routes;